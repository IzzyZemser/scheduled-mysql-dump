
// import fs from 'fs';
import AWS from 'aws-sdk';
import child_process from 'child_process'

const spawn = child_process.spawn;


//   const fileName = 'contacts.csv';

//   const uploadFile = () => {
//     fs.readFile(fileName, (err, data) => {
//        if (err) throw err;
//        const params = {
//            Bucket: 'testBucket', // pass your bucket name
//            Key: 'contacts.csv', // file will be saved as testBucket/contacts.csv
//            Body: JSON.stringify(data, null, 2)
//        };
//        s3.upload(params, function(s3Err, data) {
//            if (s3Err) throw s3Err
//            console.log(`File uploaded successfully at ${data.Location}`)
//        });
//     });
//   };
  
//   uploadFile();

const mysqlBackup = () => {
    const s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        Bucket: 'sqlBackup', // pass your bucket name
        Key: 'mysql-backup.sql' // file will be saved as testBucket/contacts.csv
      });
   
    var mysqldump = spawn(process.env.DB_PATH, [
            '-u', process.env.DB_USER_NAME,
            '-p' + process.env.DB_USER_PASSWORD,
            process.env.DB_NAME
            ]);
    return async () => {
        mysqldump
			.stdout
			.pipe(s3)
    }
}


export default mysqlBackup
